import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from yellowbrick.regressor import ResidualsPlot
# https://pypi.org/project/scikit-learn/
# https://pypi.org/project/matplotlib/
# https://pypi.org/project/numpy/
from sklearn.linear_model import LinearRegression

def main():
    print('Digite, 0 para ter previsao de um valor, 1 para entrar com uma tabela')
    op = input()
    if op == 0:
        regressaoValor()
    else:
        regressaoTabela()

def lerTeclado():
    turbidezIn = float(input('Digite a turbidez para regressao: '))
    return turbidezIn

def printa(correlacao, score, previsao):
    print('Correlacao x e y: ', correlacao[0][1])
    print('Previsao de pluviometria: ',previsao[0])
    print('Score de acerto: ',score)

def regressaoValor ():
    data = pd.read_csv('input.csv')
    x = data.iloc[:,0].values #turbidez
    y = data.iloc[:,1].values #pluviometria
    correlacao = np.corrcoef(x,y)
    x = x.reshape(-1,1)
    regressor = LinearRegression()
    regressor.fit(x,y)
    # b1
    regressor.coef_
    # b0
    regressor.intercept_
    plt.scatter(x,y)
    plt.plot(x,regressor.predict(x), color = 'red')
    plt.title('Regressao linear')
    plt.xlabel('Turbidez')
    plt.ylabel('Pluviometria')
    plt.show()
    # previsao gerada pela equacao da reta
    previsao = regressor.intercept_ + regressor.coef_ * lerTeclado()
    score = regressor.score(x,y)
    visu = ResidualsPlot(regressor)
    visu.fit(x,y)
    visu.poof()

    printa(correlacao, score, previsao)

def regressaoTabela ():
    data = pd.read_csv('input.csv')
    tabelaPrevisao = pd.read_csv('valores.csv')

    x = data.iloc[:,0].values #turbidez
    y = data.iloc[:,1].values #pluviometria

    xPre = tabelaPrevisao.iloc[:,0].values #turbidez

    correlacao = np.corrcoef(x,y)
    x = x.reshape(-1,1)
    regressor = LinearRegression()
    regressor.fit(x,y)
    # b1
    regressor.coef_
    # b0
    regressor.intercept_

    plt.scatter(x,y)
    plt.plot(x,regressor.predict(x), color = 'red')
    plt.title('Regressao linear')
    plt.xlabel('Turbidez')
    plt.ylabel('Pluviometria')
    plt.show()

    # previsao gerada pela equacao da reta
    previsao = []
    for valor in xPre:
        previsao.append(regressor.intercept_ + regressor.coef_ * valor)
    # print(previsao)
    df = pd.DataFrame(previsao)
    # COLOCAR AQUI O CAMINHO PARA O DIRETORIO DO SEU PC
    df.to_csv('/home/andre/regressao-linear/previsao.csv', header='Pluviometria', index=None)
    score = regressor.score(x,y)
    printa(correlacao, score, previsao)
main()
# regressaoTabela()
